<?php
namespace Tests\Unit\Entity\User;
use App\Entity\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
class RegisterTest extends TestCase {
    use DatabaseTransactions;
    public function testRequest(): void     {
        $user = User::register(
            $name = 'name',
            $email = 'email@mail.ru',
            $password = '123456'
        );
        self::assertNotEmpty($user);

        self::assertEquals($name, $user->name);
        self::assertEquals($email, $user->email);

        self::assertNotEmpty($user->passsword);
        self::assertNotEquals($password, $user->passsword);

        self::assertEquals(User::STATUS_WAIT, $user->status);

        self::assertFalse($user->isActive());
        //self::assertEquals($user->role == User::ROLE_USER);
        self::assertFalse($user->isAdmin());

    }

    public function testVerify(): void {
        $user = User::register('name', 'email@mail.ru', '123456' );
        /*  ****** */
        $user->verify();

        self::assertFalse($user->isWait());
        self::assertTrue($user->isActive());
    }
    public function testAlreadyVerified(): void {
        $user = User::register('name', 'email@mail.ru', '123456' );

        $user->verify();

        $this->expectExceptionMessage('User is already verified.');
        $user->verify();
    }
}
/*if ($user->status !== User::STATUS_WAIT) {
    return redirect()->route('login')
        ->with('error', 'Your email already verified.');
}
$user->status = User::STATUS_ACTIVE;
$user->verify_token = null;
$user->save();*/