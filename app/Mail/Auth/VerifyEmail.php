<?php
namespace App\Mail\Auth;
use App\Entity\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VerifyEmail extends Mailable {
    use /*Queueable,*/ SerializesModels;
    public $user;
    public function __construct(User $user)    {
        $this->user = $user;
    }
    public function build()  {
        return $this
            ->subject('Signp Confirmation')
            ->$this->markdown('email.auth.register.confirmation');
    }
}

