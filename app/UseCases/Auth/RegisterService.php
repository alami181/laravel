<?php
namespace App\UseCases\Auth;
use App\Entity\User;
use App\Http\Requests\Auth\RegisterRequest;
use App\Mail\Auth\VerifyEmail;
use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Support\Facades\Mail;

class RegisterService {
    public function register(array $data): void  {
        $user = User::register(
            $data['name'],
            $data['email'],
            $data['password']);
        Mail::to($user->email)->send(new VerifyMail($user)); //queue
        event(new Registered($user));
    }
    public function verify ($id): void {
        /** @var User $user */
        $user = User::findOrFail ($id);
        $user->verify();
    }
}