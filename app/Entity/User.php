<?php
namespace App\Entity;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
/* @property int $id
 * @property string $name
 * @property string $email
 * @property string $status
 */
class User extends Authenticatable {
    use Notifiable;
    public const STATUS_WAIT = 'wait';
    public const STATUS_ACTIVE = 'active';
    public const ROLE_USER  = 'User';
    public const SROLE_ADMIN='Admin';
    protected $fillable = [
        'name', 'email', 'password',
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];
    public static function register
        (string $name,string $email,string $password):self   {
        return static::create([ //User
            'name' => $name,
            'email' => $email,
            'password' => bcrypt($password), //Hash::make
            'verify_token' => Str::random(), //uuid()
            'status' => User::STATUS_WAIT,
        ]);
    }
    public static function new ($name,$email ) : self  {
        return static::create([ //User
            'name' => $name,
            'email' => $email,
            'verify_token' => Str::random(), //uuid()
            'status' => User::STATUS_WAIT,
        ]);
    }
    public function isWait():bool {
        return $this->status === self::STATUS_WAIT;
    }
    public function isActive():bool {
        return $this->status === self::STATUS_ACTIVE;
    }
    public function isAdmin():bool {
        return $this->role === self::ROLE_ADMIN;
    }
    public function isUser():bool {
        return $this->role === self::ROLE_USER;
    }

    public function verify() : void {
        if (!$this->isWait()) {
            throw new \DomainException('User is already verified.');
        }
        $this->update([
            'status' => User::STATUS_ACTIVE,
            'verify_token' => null,
        ]);
    }
}
