<?php

namespace App\Http\Controllers\Admin;

use App\Entity\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;

class UsersController extends Controller {
    private $register;
    /* Display a listing of the resource.
     * @return \Illuminate\Http\Response     */
    public function __construct(RegisterService $register)
    {
        $this->register = $register;
//        $this->middleware('can:manage-users');
    }
    public function index(Request $request)    {
        $query = User::orderByDesk('id');//,'desk'

        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
        }

        if (!empty($value = $request->get('name'))) {
            $query->where('name', 'like', '%' . $value . '%');
        }

        if (!empty($value = $request->get('email'))) {
            $query->where('email', 'like', '%' . $value . '%');
        }

        if (!empty($value = $request->get('status'))) {
            $query->where('status', $value);
        }

        if (!empty($value = $request->get('role'))) {
            $query->where('role', $value);
        }
        $user =  $query->pagination(20);
        $statuses = [
            User::STATUS_WAIT => 'Waiting',
            User::STATUS_ACTIVE => 'Active',
        ];
        $roles = [
            User::ROLE_USER => 'User',
            User::ROLE_ADMIN=> 'Admin',
        ];
        return view('admin.users.index', compact('user','statuses','roles'));
    }
    /* Show the form for creating a new resource.
     * @return \Illuminate\Http\Response */
    public function create()    {
        return view('admin.users.create');
    }
    /* Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response */
    public function store(Request $request)    {
        $data = $this->validate($request,[
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users'
        ]);
        $user = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'status' => User::STATUS_ACTIVE,
        ]);
        //$user = User::create($request->all());
        //$user = User::create($request->all($data));
        //$user = User::create($request->only(['name','email']));
        return redirect()->route('admin.users.show',['id'=>$user->id]);
    }
    /* Display the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response */
    public function show(User $user)    {
        //$user = User::findOrFail ($id);
        return view('admin.users.show',compact('user'));
    }
    /* Show the form for editing the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response */
    public function edit(User $user)    {
        $statuses = [User::STATUS_WAIT => 'Waining',
            User::STATUS_ACTIVE=>'Active'
        ];
        return view('admin.users.show',compact('user','statuses'));
    }
    /* Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response */
    public function update(Request $request, User $user)    {
        $data = $this->validate($request,[
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'status' => ['required','string',
                Rule::in([User::STATUS_WAIT,User::STATUS_ACTIVE]),],
        ]);
        $user->update($data);
        return redirect()->route('admin.users.show',$user);
    }
    /* Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response */
    public function destroy(User $user)    {
        $user->delete();
        return redirect()->route('admin.users.index');
    }
}
