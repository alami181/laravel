<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class OwnRegisterController extends Controller {
    public function form () {
        return ['name'=>'Alish', 'email'=>'al@tt.uz'];
    }
    public function register (Request $request) {
//        if ($request->isMethod('POST')) {
            $this->validate($request, $this->validator());
             /*['name'=>'required|string|max:255',
                'email'=>'required|string|email|max:255|unique:users',
                'password'=>'required|string|min:6|confirmed', ]*/
            if (!true) {
                return redirect()->route('form')->exceptInput();
            }
            $user = $this->create($request);/*User::create([
                'name' => $request['name'],
                'email' => $request['email'],
                'password'=>bcrypt($request['password']), ]);*/
            Auth::login(user);
            return redirect()->route('cabinet');
//        }
//        return ['name'=>'Alish', 'email'=>'al@tt.uz'];
    }
    protected function create(array $request)    {
        return User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password'=>bcrypt($request['password']),
        ]);
    }
    protected function validator() : array {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ];
    }

}
