<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\Auth\RegisterRequest;
use App\Mail\VerifyMail;
use App\Entity\User;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use \Illuminate\Support\Str;

class RegisterController extends Controller {
    use RegistersUsers;
    protected $redirectTo = '/cabinet';//home
    public function __construct()  {
        $this->middleware('guest');
    }
    public function showRegistrationForm() {
        return view('auth.register');
    }
    public function register (RegisterRequest $request) {
        $service = new RegisterService();
        $user = $service->register ($request);
        //$this->validate($request, );--- можно не вызывать валидацию, естьв форме
//        $user = User::register(
//                  $request['name'],
//                  $request['email'],
//                  $request['password']);
          /*create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']), //Hash::make
            'verify_token' => Str::random(), //uuid()
            'status' => User::STATUS_WAIT,
        ]);*/

//        Mail::to($user->email)->queue(new VerifyMail($user)); //send
//        event(new Registered($user));
        return redirect()->route('login')
            ->with('success', 'Check your email and clock on the link to verify');
    }
    public function verify($token) {
        if (!$user = User::where('verify_token', $token)->first()) {
            return redirect()->route('login')
                ->with('error', 'Sorry your link cannot be identified.');
        }
        try {
            $user->verify();//$this->service->verify($user->id);
            return redirect()->route('login')
                ->with('success', 'Your e-mail is verified. You can now login.');
        } catch (\DomainException $e) {
            return redirect()->route('login')->with('error', $e->getMessage());
        }
        /*if ($user->status !== User::STATUS_WAIT) { //эти проверки уже есть
            return redirect()->route('login')
                ->with('error', 'Your email already verified.');
        }
        $user->status = User::STATUS_ACTIVE;
        $user->verify_token = null;
        $user->save();
        return redirect()->route('login')
            ->with('success', 'Your e-mail is verified. You can now login.');*/
    }
    protected function validator(array $data)  {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }
    protected function create(array $data)   {
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']), //Hash::make
            'verify_token' => Str::random(), //uuid()
            'status' => User::STATUS_WAIT,
        ]);
        Mail::to($user->email)->queue(new VerifyMail($user)); //send
        return $user;
    }
    protected function registered (Request $request, $user) {
        $this->guard()->logout();
        return redirect()->route('login')
            ->with('success', 'Check your email and clock on the link to verify');
    }
}
