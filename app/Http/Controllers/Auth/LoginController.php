<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Entity\User;
use Auth;
use Dotenv\Exception\ValidationException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller {
    use AuthenticatesUsers;
    protected $redirectTo = '/cabinet';//home
    public function __construct()    {
        $this->middleware('guest')->except('logout');
    }
    public function showLoginForm() {
        return view('auth.login');
    }
    public function register (LoginRequest $request) {
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            $this->sendLockoutResponse($request);
        }
        $authenticate = Auth::attempt(
            $request->only(['email','password']),
            $request->filled('remember')
        );
        if ($authenticate) {
            $request->session()->regenerate();
            $this->clearLoginAttempts($request);
            $user = Auth::user();
            if (!$user->status != User::STATUS_ACTIVE) {
                Auth::logout();
                return back()->with('error',' You need confirm account. Pls check email');
            }
            return redirect()->intended(route('cabinet'));
        }
        $this->incrementLoginAttempts($request);
        throw ValidationException::withMessages(['email'=>[trans('auth_failed')]]);
    }

    protected function authenticated(Request $request, $user) {
        if (!$user->status !== User::STATUS_ACTIVE) {
            $this->guard()->logout();  // накостылить-разлогинить
            return redirect()->route('login')->with('success', 'Check email&click link to verify');
        }
        return redirect()->intended($this->redirectPath());
    }
    public function logout(Request $request)    {
        Auth::/*guard()->*/logout();
        $request->session()->invalidate();//перебиваем/очищаем сессию
        return redirect()->route('home');
    }
    protected function username() {return 'name';}//переопередяем
}
