<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest{
    public function authorize() :bool   {
        return true;//false;
    }

    public function rules()   {
        return [
            'email' => 'required|string',
            'password' => 'required|string',
        ];
    }
}
