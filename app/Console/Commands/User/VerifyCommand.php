<?php

namespace App\Console\Commands\User;

use App\Entity\User;
use App\UseCases\Auth\RegisterService;
use Illuminate\Console\Command;

class VerifyCommand extends Command  {
    protected $signature = 'user:verify {email}';
    protected $description = 'Command description';
    public function handle() :bool    {
        $email = $this->argument('email');
        if (!$user = User::where('email', $email)->first()) {
            $this->error('Undefined user with email ',$email);
            return false;
        }
        /*$user->status = User::STATUS_ACTIVE;
        $user->verify_token = null;
        $user->save();*/
        /*$user->update([
            $user->status = User::STATUS_ACTIVE,
            $user->verify_token = null,
        ]);*/
        try {
            $this->service->verify($user->id);
        } catch (\DomainException $e) {
            $this->error($e->getMessage());
        }
        $this->info('Success <error>'.$email.'</error> '.$user);//
        return true;
    }
    private $service;
    public function __construct(RegisterService $service)  {
        parent::__construct();
        $this->service = $service;
    }
}
