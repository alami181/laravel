<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();
//Route::get('/ownregister', 'Auth\OwnRegisterController@form')->name('ownregister ');
//Route::post('/ownregister', 'Auth\OwnRegisterController@register');
Route::get('/verify/{token}', 'Auth\RegisterController@verify')->name('register.verify');
Route::get('/cabinet', 'Cabinet\HomeController@index')->name('cabinet');

Route::prefix('admin')->group(function () {
    Route::middleware('auth')->group(function () {
        Route::namespace('Admin')->group(function () {
            Route::get('/admin', 'Admin\HomeController@index')->name('admin.home');
            Route::resource('users', 'UsersControllers');
        });
    });
});

Route::group([
    'prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin',
    'middleware' => 'auth'
    ], function () {
        Route::get('/admin', 'HomeController@index')->name('home');
        Route::resource('users', 'UsersController');
        Route::post('/users/{user}/verify', 'UsersController@verify')->name('home.verify');    }
 );
