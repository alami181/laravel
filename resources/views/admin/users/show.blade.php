@extends('layouts.app')

@section('content')
    @include('admin.users._nav')
    <div class="d-flex flex-row mp-3">
        <a href="{{ route ('admin.users.edit', $user) }}" class="btn btn-primary mr-1">Edir</a>
        <form method="POST">
            @csrf
            @method('DELETE')
            <button class="btn btn-danger">Delete</button>
        </form>
    </div>
    <table class="table table-bordered table-striped">
        <tbody>
            <tr>
                <th>ID</th><td>{{ $user->id }}</td>
                <th>Name</th><td>{{ $user->name }}</td>
                <th>Email</th><td>{{ $user->email }}</td>
                <th>Status</th><td>
                    @if($user->isWait())
                        <span class="badge badge-secondary">Waiting</span>
                    @endif
                    @if($user->status === \App\Entity\User::STATUS_ACTIVE)
                        <span class="badge badge-primary">Active</span>
                    @endif
                </td>
            </tr>
        </tbody>
    </table>
@endsection
