@extends('@layouts.app)

@section('content')
    <ul class="nav nav-tabs mo-3">
        <li class="nav-item"><a class="nav-link active" href="{{ route('admin.name') }}">Dashboard</a> </li>
        <li class="nav-item"><a class="nav-link" href="{{ route('admin.users.index') }}">Users</a> </li>
    </ul>
@endsection